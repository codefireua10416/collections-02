/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ArrayList<Employee> list = new  ArrayList<>();
        
        list.add(new Employee("Vasya Pupkin 1", 3000.0));
        list.add(new Employee("Vasya Pupkin 6", 3000.0));
        list.add(new Employee("Vasya Pupkin 2", 4000.0));
        list.add(new Employee("Vasya Pupkin 3", 2000.0));
        list.add(new Employee("Vasya Pupkin 4", 3000.0));
        list.add(new Employee("Vasya Pupkin 5", 1000.0));
        
        System.out.println("LIST (UNSORTED)");
        for (Employee worker : list) {
            System.out.println(worker);
        }
        
        Collections.sort(list);
        
        System.out.println("LIST (SORTED)");
        for (Employee worker : list) {
            System.out.println(worker);
        }
        
        System.out.println("SET (UNIQ SORTED)");
        TreeSet<Employee> workers = new TreeSet<>(list);
        
        for (Employee worker : workers) {
            System.out.println(worker);
        }
        
        System.out.println("Custom get");
        System.out.println(workers.first());
        System.out.println(workers.last());
        System.out.println(workers.last().getSalary() + workers.first().getSalary());
        
    }

}
