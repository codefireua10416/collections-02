/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set<Employee> workers = new HashSet<>();
        
        workers.add(new Employee("Vasya Pupkin", 3000.0));
        workers.add(new Employee("Vasya Pupkin", 3000.0));
        workers.add(new Employee("Vasya Pupkin", 3000.0));
        workers.add(new Employee("Vasya Pupkin", 3000.0));
        workers.add(new Employee("Vasya Pupkin", 3000.0));
        
        System.out.println(workers);
    }

}
