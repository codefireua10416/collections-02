/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

/**
 *
 * @author human
 */
public class Employee {

    private String name;
    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public int hashCode() {
        return name.hashCode() * 67 + 2;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        if (obj.getClass() == Employee.class) {
            Employee employee = (Employee)obj;
            
            if (employee.name.equals(this.name) && employee.salary == this.salary) {
                return true;
            }
        }
        
        return super.equals(obj);
    }
}
