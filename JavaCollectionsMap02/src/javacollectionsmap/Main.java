/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollectionsmap;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String, Double> salary = new LinkedHashMap<>();

        salary.put("Vasya Pupkin #1", 1000.00);
        salary.put("Vasya Pupkin #2", 2000.00);
        salary.put("Vasya Pupkin #3", 3000.00);

        System.out.println("Key SET");
        Set<String> keySet = salary.keySet();
        
        for (String key : keySet) {
            System.out.printf("%s = %s\n", key, salary.get(key));
        }
        
        System.out.println("Entry SET");
        Set<Map.Entry<String, Double>> entrySet = salary.entrySet();
        
        for (Map.Entry<String, Double> entry : entrySet) {
            System.out.printf("%s : %s\n", entry.getKey(), entry.getValue());
            
            if (entry.getValue() < 2000.) {
                entry.setValue(entry.getValue() * 2);
            }
        }
        
        double summ = 0;
        
        Collection<Double> values = salary.values();
        for (Double value : values) {
            summ += value;
        }
        
        System.out.println("SUMM: " + summ);
    }

}
