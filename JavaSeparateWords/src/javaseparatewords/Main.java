/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaseparatewords;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> wordList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            
            if (nextLine.equalsIgnoreCase("end")) {
                break;
            }
            
            //
            String[] words = nextLine.split("[\\s,\\.\\!\\?\\'\\\"]+");
            
            List<String> asList = Arrays.asList(words);
            
            wordList.addAll(asList);
        }
        
        System.out.println(wordList);
        
        
        
    }
    
}
