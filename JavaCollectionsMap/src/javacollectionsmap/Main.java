/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollectionsmap;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String, Double> salary = new HashMap<>();

        System.out.println(salary.containsKey("Vasya Pupkin"));

        System.out.println(salary.put("Vasya Pupkin", 1000.00));
        System.out.println(salary.put("Vasya Pupkin", 2000.00));
        System.out.println(salary.put("Vasya Pupkin", 3000.00));

        System.out.println(salary.size());
        System.out.println(salary.get("Vasya Pupkin"));

        System.out.println(salary.containsKey("Vasya Pupkin"));
    }

}
