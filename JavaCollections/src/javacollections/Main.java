/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacollections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set<String> numbers = new HashSet<>();

        numbers.add("One");
        numbers.add("Two");
        numbers.add("Three");
        numbers.add("Four");
        numbers.add("Five");

        numbers.add("Five");
        numbers.add("Four");
        numbers.add("Three");
        numbers.add("Two");
        numbers.add("One");

        System.out.println(numbers);

        Iterator<String> iterator = numbers.iterator();

        while (iterator.hasNext()) {
            String next = iterator.next();
            if (next.toLowerCase().startsWith("t")) {
                iterator.remove();

                System.out.print("x ");
            }

            System.out.println(next);
        }
        
        System.out.println("::for-each::");
        
        for (String number : numbers) {
            System.out.println(number);
        }
    }

}
